var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
const mongoose = require('mongoose');
const keys = require('./config/keys');
require('./models/User');
require('./services/passport');

var hostname = 'localhost';
var PORT = process.env.PROT || 5000;

mongoose.connect(keys.mongoURI);

var app = express();
app.use(bodyParser.json());
app.use(passport.initialize());
//app.use(passport.session());
require('./routes/authRoutes')(app);


app.get('/*', (req, res) => {
	res.status(404).send('404 oups');
})



// Démarrer le serveur

var server = app.listen(PORT, hostname, function () {

	console.log("Mon serveur fonctionne sur http://" + hostname + ":" + PORT + "\n");

});

module.exports = server;