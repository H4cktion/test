var  Tool = require("../components/tool");

describe("Test CRUD tools",()=>{
    var tool;

    beforeEach(()=>{
        tool = new Tool();
        tool.addNewTool({ name:"marteau", photo:"url", lend:false, actualOwner:"mySelf" });
        tool.addNewTool({ name:"pince à sertir", photo:"url", lend:true, actualOwner:"maxime" });
    });

    afterEach(()=>{
        tool = [];
    })

    describe("Add tool to the toolList",()=>{ 
        it("add tool",()=>{
            expect(tool.getTools()).isNotNull;
            expect(tool.getTools().length).toEqual(2);
        });
    });

    describe("remove tool to the toolList",()=>{ 
        
        it("shouldRemoveOneTool_whenCallRemoveToolFunction",()=>{
            tool.removeTool("marteau");
            expect(tool.getTools().length).toEqual(1);
        });
    });

    describe("know if tool is lend",()=>{ 
        
        it("shouldReturnTrue_whenToolIsLend",()=>{
            expect(tool.isLended("pince à sertir")).toBe(true);
        });
        it("shouldReturnFalse_whenToolIsLend",()=>{
            expect(tool.isLended("marteau")).toBe(false);
        });
    });

    describe("get actual owner",()=>{ 
        
        it("shouldReturnActualOnwerName",()=>{
            expect(tool.getActualOwner("pince à sertir")).toBe("maxime");
        });
    });

    describe("lend tool",()=>{
        it("souldPassLendToTrueAndChangeActualOwner_whenLendTool",()=>{
            tool.lend("marteau","bruno");
            expect(tool.getActualOwner("marteau")).toBe("bruno");
            expect(tool.isLended("marteau")).toBe(true);
        })
    });

    describe("retrived tool",()=>{
        it("shouldPassLendToFalseAndChangeActualOwner_whenRetirvedTool", ()=>{
            tool.retrived("pince à sertir");
            expect(tool.getActualOwner("pince à sertir")).toBe("mySelf");
            expect(tool.isLended("pince à sertir")).toBe(false);

        })
    })


});