var Request = require('Request');

describe("Serveur testing",()=>{
    var server;

    beforeAll(()=>{
        server = require("../serveur");
    });
    afterAll(() => {
        server.close();
    });

    describe("Get /ahah (wrong uri)",()=>{
        var data = {};
        beforeAll((done)=>{
            Request.get("http://localhost:5000/ahah", (error,response,body)=>{
                data.status = response.statusCode;
                data.body = body;
                done();
            });
        });

        it("shouldResponse404StatusAndErrorMessage_whenCallingServerWithWrongUri",()=>{
            expect(data.status).toBe(404);
            expect(data.body).toBe("404 oups");
        });


    })
})