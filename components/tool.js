"use strict";

const MY_SELF = "mySelf";

 class tool{

    constructor(){
        this.tools = [];
    }
    
    addNewTool(tool){
        this.tools.push(tool);
    }

    removeTool(toolToRemove){
        this.tools = this.tools.filter(tool =>  tool.name != toolToRemove);

    }

    isLended(toolName){
        return this.tools.find(tool => tool.name == toolName).lend;
    }

    getActualOwner(toolName){
        return this.tools.find(tool => tool.name == toolName).actualOwner
    }

    getTools(){
        return this.tools;
    }

    lend(toolName,newOwner){
        this.tools = this.tools.map( tool => {
                        if(tool.name == toolName){
                            tool.lend = true;
                            tool.actualOwner = newOwner;
                        }
                        return tool;
        });
    }

    retrived(toolName){
        this.tools = this.tools.map( tool => {
            if(tool.name == toolName){
                tool.lend = false;
                tool.actualOwner = MY_SELF;
            }
            return tool;
        });
    }

}

module.exports = tool;
