const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const toolSchema = new Schema({
    name: String,
    photo: String,
    lend: Boolean,
    actualOwner: String
});

mongoose.model('tools', toolSchema);

