
var passport = require('passport');

module.exports = (app) => {
    app.post('/login',
	passport.authenticate('local'),
	function (req, res) {
		// If this function gets called, authentication was successful.
		// `req.user` contains the authenticated user.
		res.status(200).send(req.user);
		//res.redirect('/user/' + req.user.id);
	});
}
